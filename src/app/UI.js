export class UI {
    constructor(){
        this.location = document.getElementById('weather-location');
        this.desc = document.getElementById('weather-description');
        this.string = document.getElementById('weather-string');
        this.humidity = document.getElementById('weather-humidity');
        this.wind = document.getElementById('weather-wind');
        this.windKm = document.getElementById('weather-windKm');
    }
    render(weather){
        this.location.textContent = weather.name + '/' + weather.sys.country;
        this.desc.textContent = weather.weather[0]['description'];
        this.string.textContent = weather.main.temp + ' °C';
        this.humidity.textContent = 'Humidity: ' + weather.main.humidity + '%';
        this.wind.textContent = 'Wind in MS: ' + weather.wind.speed + 'M/S';
        this.windKm.textContent = 'Wind in KM: ' + Math.round(weather.wind.speed * 3.6) + 'K/M';

    }
}