# weather-1.0-app
This is a normal weather app, where you can get the weather, humidity and wind speed of an X (country) and its X (city).This is a normal weather app, where you can get the weather, humidity and wind speed of an X (country) and its X (city). TOOLS: -Webpack -OpenWeather API -Node JS.

WEBSITE: https://lucabecci.github.io/weather-1.0-app/dist/index.html

PICS:
Córdoba, Argentina:
![Cordoba, ARG](https://github.com/lucabecci/weather-1.0-app/blob/master/cordoba.png)

París, Francia:
![Paris, FR](https://github.com/lucabecci/weather-1.0-app/blob/master/paris.png)
